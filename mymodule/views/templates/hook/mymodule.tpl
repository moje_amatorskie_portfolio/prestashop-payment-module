<!-- Block mymodule -->
<div id="mymodule_block_home" class="block" >
  <h4 hidden>{l s='Welcome!' mod='mymodule'}</h4>
  <div class="block_content">
    <ul>
      <li hidden><a href="{$my_module_link}" title="Click this link">Click me!</a></li>
    </ul>
    {if isset($message) && $message}
    <h1>
      <div class="alert-success " style="text-align:center;font-size:44px;">
        {$message}
      </div>
    </h1>
    {/if}
  </div>
</div>
<!-- /Block mymodule -->
