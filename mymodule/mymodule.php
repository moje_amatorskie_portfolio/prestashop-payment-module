<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class MyModule extends Module
{



  // public function hookActionAdministrationPageForm(&$hookParams)
  // {
  //     $formBuilder = $hookParams['form_builder'];
  //     $uploadQuotaForm = $formBuilder->get('upload_quota');
  //     $uploadQuotaForm->add(
  //         'description',
  //         TextType::class,
  //         [
  //             'data' => 'A description',
  //             'label' => 'Description'
  //         ]
  //     );
  // }
  //
  // public function hookActionAdministrationPageSave(&$hookParams)
  // {
  //     // retrieve and validate the data
  //     dump($hookParams['form_data']['upload_quota']['description']);
  //
  //     // if the data is invalid, populate `errors` array
  //     dump($hookParams['errors']);
  // }



//   public $tabs = array(
//   array(
//       'name' => array(
//           'en' => 'angliczi picipolo', // Default value should be first
//           'fr' => 'france picipolo',
//           'pl' => 'mojmodulpicipolo'
//       ),
//       'class_name' => 'AdminGamification',
//       // 'parent_class_name' => 'ShopParameters',
// ));

    public function getContent() //The getContent() method is the first one to be called when the configuration
    // page is loaded.
    {
        $output = null;

        if (Tools::isSubmit('submit'.$this->name)) {
            $myModuleName = strval(Tools::getValue('MYMODULE_NAME'));

            if (
                !$myModuleName ||
                empty($myModuleName) ||
                !Validate::isGenericName($myModuleName)
            ) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                Configuration::updateValue('MYMODULE_NAME', $myModuleName); // to tutaj updates database after module form submission
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        return $output.$this->displayForm();
    }


















    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fieldsForm[0]['form'] = [
        'legend' => [
            'title' => $this->l('Settings'),
        ],
        'input' => [
            [
                'type' => 'text',
                'label' => $this->l('Configuration value'),
                'name' => 'MYMODULE_NAME',
                'size' => 20,
                'required' => true
            ]
        ],
        'submit' => [
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right'
        ]
    ];

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
        'save' => [
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ],
        'back' => [
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        ],
    ];

        // Load current value
        $helper->fields_value['MYMODULE_NAME'] = Configuration::get('MYMODULE_NAME');

        return $helper->generateForm($fieldsForm);
    }


















    public function __construct()
    {
        $this->name = 'mymodule';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Bolek Bolkowski';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('My module');
        $this->description = $this->l('Description of gieniowy module.');

        $this->confirmUninstall = $this->l('Are you suree you want to uninstall?');

        if (!Configuration::get('MYMODULE_NAME')) {
            $this->warning = $this->l('No name provided');
        }
    }












    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }



        return parent::install() &&
            // $this->registerHook('leftColumn') &&
            // $this->registerHook('displayHome') && //hyba moze byc i samo `home` ale nie jestem pewien
            $this->registerHook('header') &&
            $this->registerHook('top') &&  //przy instalacji modulu juz wtedy
            //nie trza transplant bo samo sie wsadzi, np. jak tu jest `'top'` to sie wsadzi w hook  o nazwie displayTop
            Configuration::updateValue('MYMODULE_NAME', 'my friend');
    }





















    // public function hookDisplayLeftColumn($params)   //ta funkcja wsadza moduł we front office i
    // //umozliwia transplant do tego hooka czyli do `displayleftcolumn`
    // {
    //     $message = Tools::getValue('message');
    //     $this->context->smarty->assign([
    //     'my_module_name' => Configuration::get('MYMODULE_NAME'),   //sets smarty template name nam
    //     'my_module_link' => $this->context->link->getModuleLink('mymodule', 'display'),
    //     'message' => $message,
    //   ]);
    //
    //     return $this->display(__FILE__, 'mymodule.tpl');
    // }

    // public function hookDisplayRightColumn($params)
    // {
    //     return $this->hookDisplayLeftColumn($params);
    // }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'css/mymodule.css', 'all');
    }



    // public function hookDisplayHome($params)  //ta funkcja wsadza moduł we front office i
    // //umozliwia transplant do tego hooka czyli do `displayHome`
    // {
    //   $message = Tools::getValue('message');
    //   $this->context->smarty->assign([
    //   'my_module_name' => Configuration::get('MYMODULE_NAME'),   //sets smarty template name nam
    //   'my_module_link' => $this->context->link->getModuleLink('mymodule', 'display'),
    //   'message' => $message,
    // ]);
    //
    //   return $this->display(__FILE__, 'mymodule.tpl');
    // }

    public function hookDisplayTop($params)
    {
      require 'dane.php';
      $message = Tools::getValue('message');
      $this->context->smarty->assign([
      'my_module_name' => 'hookujemy na top',   //sets smarty template name nam
      'my_module_link' => $this->context->link->getModuleLink('mymodule', 'display'),
      'message' => $message,
    ]);

      return $this->display(__FILE__, 'mymodule.tpl');
    }












    public function uninstall()
    {
        if (!parent::uninstall() ||
        !Configuration::deleteByName('MYMODULE_NAME')
    ) {
            return false;
        }

        return true;
    }
}
